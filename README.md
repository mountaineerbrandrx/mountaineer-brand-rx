Mountaineer Brand RX was founded in 2018 by Eric Young and Tenise Baskett. All Mountaineer Brand RX products are carefully made only from the highest quality, all natural, and organic raw materials in its own manufacturing facility in West Virginia. All CBD is US-sourced and third party tested.

Address: 54 General Motors Access Rd, Suite R, Martinsburg, WV 25403, USA

Phone: 304-551-0250

Website: https://mountaineerbrandrx.com
